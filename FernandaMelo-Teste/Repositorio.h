//
//  Repositótios.h
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 23/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//

//Cada repositório deve exibir Nome do repositório, Descrição do Repositório, Nome / Foto do autor, Número de Stars, Número de Forks

#import <Foundation/Foundation.h>

@interface Repositorio : NSObject {
    NSString* repositNome;
    NSString* repositDescricao;
    NSString* repositAutorNome;
    NSString* repositAutorFoto;
    int  repositStars;
    int  repositForks;
    
}



@property (nonatomic,strong) NSString* repositNome;
@property (nonatomic,strong) NSString* repositDescricao;
@property (nonatomic,strong) NSString* repositAutorNome;
@property (nonatomic,strong) NSString* repositAutorFoto;
@property (nonatomic, assign) int  repositStars;
@property (nonatomic, assign) int  repositForks;


-(Repositorio*) initRepositorioNome: (NSString*) nome eDescricao: (NSString*) descricao eAutorNome: (NSString*) autorNome eAutorFoto: (NSString*) autorFoto eStars: (int) stars eForks: (int) forks;

@end
