//
//  PullTableViewCell.h
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 24/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PullTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *tituloPull;
@property (weak, nonatomic) IBOutlet UILabel *descricaoPull;
@property (weak, nonatomic) IBOutlet UILabel *dataPull;
@property (weak, nonatomic) IBOutlet UIImageView *fotoAutorPull;
@property (weak, nonatomic) IBOutlet UILabel *nomeAutorPull;

@end
