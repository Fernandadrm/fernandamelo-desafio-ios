//
//  PullTableViewCell.m
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 24/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//

#import "PullTableViewCell.h"

@implementation PullTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    //deixa imagem redonda 
    self.fotoAutorPull.layer.cornerRadius = self.fotoAutorPull.frame.size.width / 2;
    self.fotoAutorPull.clipsToBounds = YES;
    self.fotoAutorPull.layer.borderWidth = 3.0f;
    self.fotoAutorPull.layer.borderColor = [UIColor orangeColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
