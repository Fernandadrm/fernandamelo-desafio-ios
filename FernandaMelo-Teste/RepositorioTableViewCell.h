//
//  RepositorioTableViewCell.h
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 24/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
@interface RepositorioTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nomeRepositorio;
@property (weak, nonatomic) IBOutlet UITextView *descricaoRepositorio;

@property (weak, nonatomic) IBOutlet UILabel *forkRepositorio;

@property (weak, nonatomic) IBOutlet UILabel *starsRepositorio;
@property (weak, nonatomic) IBOutlet UILabel *nomeUsuarioRepositorio;
@property (weak, nonatomic) IBOutlet UILabel *nomeSobrenomeRepositorio;

@property (weak, nonatomic) IBOutlet UIImageView *imagemUsuarioRepositorio;

@end
