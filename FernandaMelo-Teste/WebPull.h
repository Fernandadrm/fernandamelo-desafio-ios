//
//  WebPull.h
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 25/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebPull : UIViewController

@property (nonatomic) NSString* pullSelecionado;
@property (nonatomic) NSString* titulo;
@property (weak, nonatomic) IBOutlet UIWebView *web;

@end
