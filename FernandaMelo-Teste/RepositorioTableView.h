//
//  RepositorioTableView.h
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 24/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "Repositorio.h"
#import "RepositorioTableViewCell.h"
#import "PullTableViewController.h"
@interface RepositorioTableView : UITableViewController

@property (nonatomic) NSMutableArray* repositorios;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *carregandoDados;

-(void)getListaRepositorios;
-(void)mensagemAlert: (NSString*) mensagem eTitulo: (NSString*) titulo;
-(void)verifica:(NSString*)string;
-(void)initArray:(NSArray*) arrayResult;
@end
