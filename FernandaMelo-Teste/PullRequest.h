//
//  PullRequest.h
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 23/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//
//Cada item da lista deve exibir Nome / Foto do autor do PR, Título do PR, Data do PR e Body do PR
//Ao tocar em um item, deve abrir no browser a página do Pull Request em questão

#import <Foundation/Foundation.h>

@interface PullRequest : NSObject

@property (nonatomic,strong) NSString* pullAutorNome;
@property (nonatomic,strong) NSString* pullAutorFoto;
@property (nonatomic,strong) NSString* pullTitulo;
@property (nonatomic,strong) NSString* pullData;
@property (nonatomic,strong) NSString* pullDescricao;
@property (nonatomic,strong) NSString* pullURL;

@end
