//
//  PullTableViewController.m
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 24/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//

#import "PullTableViewController.h"


@interface PullTableViewController ()

@end

@implementation PullTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.titulo != nil) {
       self.navigationItem.title = self.titulo;
    }
    
    [self dataJson: @""];
    [self getListaPullRequest];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.pullRequests.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     PullTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellPull" forIndexPath:indexPath];
    PullRequest* pull = [self.pullRequests objectAtIndex:indexPath.row];
    
    // verifica se dado é do tipo NSNull ou nil e trata
    if (pull.pullTitulo == nil || [pull.pullTitulo isEqual:[NSNull null]]) {
        pull.pullTitulo = @"";
    }
    cell.tituloPull.text = pull.pullTitulo;
    if (pull.pullAutorFoto == nil || [pull.pullAutorFoto isEqual:[NSNull null]]) {
    }else{
        [cell.fotoAutorPull sd_setImageWithURL:[NSURL URLWithString:pull.pullAutorFoto] placeholderImage:[UIImage imageNamed:@"nuvemazul.png"]];
    }
    if (pull.pullData == nil || [pull.pullData isEqual:[NSNull null]]) {
        pull.pullData = [NSDate init];
    }
    cell.dataPull.text = pull.pullData;
    if (pull.pullDescricao == nil || [pull.pullDescricao isEqual:[NSNull null]]) {
        cell.descricaoPull.text = @"";
    }
    cell.descricaoPull.text = pull.pullDescricao;
    if (pull.pullAutorNome == nil || [pull.pullAutorNome isEqual:[NSNull null]]) {
        pull.pullAutorNome = @"";
    }
    
    cell.nomeAutorPull.text = pull.pullAutorNome;
    
    return cell;
}

//trata data em formato string
-(NSString*)dataJson: (NSString*) dataString{

    NSString *dateReceivedInString = @"2014-05-07T10:28:52.000Z";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *data = [dateFormatter dateFromString:dateReceivedInString];
    
    return [NSDateFormatter localizedStringFromDate:data dateStyle:NSDateFormatterMediumStyle timeStyle:nil];
    
}

// metodo utiliza AFNetworking para fazer requisições
-(void)getListaPullRequest{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSString *formatoURL = [[NSString alloc] initWithFormat:@"https://api.github.com/repos/%@/pulls",self.repositSelecionado];
    
    NSURL *URL = [NSURL URLWithString:formatoURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error.localizedDescription);
            [self mensagemAlert:@"Ops!" eTitulo:@"Tivemos um erro ao buscar os PullRequests"];
        } else {
            if (self.pullRequests == nil) {
                self.pullRequests =[[NSMutableArray alloc]init];
            }
            //Pega o os valores
            NSArray *resultArray = responseObject;
            if (resultArray.count > 0) {
                [self initArrayResult: resultArray];
            }else{
                [self mensagemAlert:@"Opa!" eTitulo:@"Reste Repositorio não possui PullRequests até o momento!"];
                return;
            }
        }
    }];
    [dataTask resume];
    
}

//passa o resuldato do JSon para um arry de objetos da classe PullRequest
-(void)initArrayResult: (NSArray*) resultArray{
    
    for (NSDictionary* atual in resultArray) {
        PullRequest* pull = [[PullRequest alloc]init];
        if (pull != nil)
        {
            pull.pullTitulo = [atual valueForKey:@"title"];
            pull.pullDescricao = [atual valueForKey:@"body"];
            pull.pullURL = [atual valueForKey:@"html_url"];
            pull.pullData = [self dataJson:[atual valueForKey:@"updated_at"]];
            pull.pullAutorFoto  =  [[atual valueForKey:@"user"] valueForKey:@"avatar_url"];
            pull.pullAutorNome = [[atual valueForKey:@"user"] valueForKey:@"login"];
            [self.pullRequests addObject:pull];
        }
        
    }
    
    [self.tableView reloadData];
}

-(void)mensagemAlert: (NSString*) mensagem eTitulo: (NSString*) titulo{
    UIAlertController *alerta = [UIAlertController alertControllerWithTitle:titulo message:mensagem preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    
    [alerta addAction:ok];

    [self presentViewController: alerta animated: YES completion: nil];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"webPull"]) {
        
        WebPull *webPull = [segue destinationViewController];
        PullRequest* pull =  [self.pullRequests objectAtIndex: [self.tableView indexPathForSelectedRow].row];
        if (pull.pullURL == nil || [pull.pullURL isEqual:[NSNull null]]) {
            [self mensagemAlert:@"Infelizmente não foi encontrada a URL de Pull" eTitulo:@"OPS!"];
            return;
        }
        webPull.pullSelecionado = pull.pullURL;
        webPull.titulo = pull.pullTitulo;
    }

}


@end
