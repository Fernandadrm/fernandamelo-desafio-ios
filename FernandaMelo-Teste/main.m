//
//  main.m
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 22/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
