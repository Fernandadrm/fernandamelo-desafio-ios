//
//  AppDelegate.h
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 22/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

