//
//  WebPull.m
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 25/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//

#import "WebPull.h"

@interface WebPull ()

@end

@implementation WebPull

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *urlString = self.pullSelecionado;
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.web loadRequest:urlRequest];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
