//
//  RepositorioTableViewCell.m
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 24/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//

#import "RepositorioTableViewCell.h"

@implementation RepositorioTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // apresenta a imagem redonda
    self.imagemUsuarioRepositorio.layer.cornerRadius = self.imagemUsuarioRepositorio.frame.size.width / 2;
    self.imagemUsuarioRepositorio.clipsToBounds = YES;
    self.imagemUsuarioRepositorio.layer.borderWidth = 3.0f;
    self.imagemUsuarioRepositorio.layer.borderColor = [UIColor orangeColor].CGColor;
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
