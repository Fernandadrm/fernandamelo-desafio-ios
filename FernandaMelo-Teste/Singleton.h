//
//  Singleton.h
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 24/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFURLSessionManager.h"
#import "AFNetworking.h"
#import "AFNetworking.h"
@interface Singleton : NSObject

@property(nonatomic) int page;
@property(nonatomic) int fimRequest;

+(instancetype)sharedInstance;


@end
