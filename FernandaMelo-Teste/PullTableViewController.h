//
//  PullTableViewController.h
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 24/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequest.h"
#import "UIImageView+WebCache.h"
#import "Singleton.h"
#import "PullRequest.h"
#import "PullTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "WebPull.h"

@interface PullTableViewController : UITableViewController

@property (nonatomic) NSString* repositSelecionado;
@property (nonatomic) NSString* titulo;
@property (nonatomic) NSMutableArray* pullRequests;

-(void)initArrayResult: (NSArray*) resultArray;
-(NSString*)dataJson: (NSString*) dataString;
@end
