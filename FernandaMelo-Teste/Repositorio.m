//
//  Repositótios.m
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 23/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//


#import "Repositorio.h"

@implementation Repositorio

@synthesize repositNome, repositDescricao, repositAutorNome, repositAutorFoto, repositStars, repositForks;

-(Repositorio*) initRepositorioNome: (NSString*) nome eDescricao: (NSString*) descricao eAutorNome: (NSString*) autorNome eAutorFoto: (NSString*) autorFoto eStars: (int) stars eForks: (int) forks{
    
    Repositorio* repositorio = [[Repositorio alloc]init];
    
    repositorio.repositNome = nome;
    repositorio.repositDescricao = descricao;
    repositorio.repositAutorNome = autorNome;
    repositorio.repositAutorFoto = autorFoto;
    repositorio.repositStars = stars;
    repositorio.repositForks = forks;

    return repositorio;

}


@end
