//
//  RepositorioTableView.m
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 24/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//

#import "RepositorioTableView.h"

@interface RepositorioTableView ()

@end

@implementation RepositorioTableView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //vefica se chegou ao fim das requisições
    if (![Singleton sharedInstance].fimRequest) {
        [self getListaRepositorios];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.repositorios.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RepositorioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"CellRepositorio"forIndexPath:indexPath];
    
    if (indexPath.row < self.repositorios.count - 6) {
        if (indexPath.row <= self.repositorios.count) {
            Repositorio* repositorio =  [self.repositorios objectAtIndex: indexPath.row];
            
            // verifica se dado é NSNull ou nil
            if (repositorio.repositNome == nil || [repositorio.repositNome isEqual:[NSNull null]]){
                repositorio.repositNome = @"";
            }
            cell.nomeRepositorio.text = repositorio.repositNome;
            if (repositorio.repositDescricao == nil || [repositorio.repositDescricao isEqual:[NSNull null]]) {
                repositorio.repositDescricao = @"";
            }
            cell.descricaoRepositorio.text = repositorio.repositDescricao;
            if (repositorio.repositAutorNome == nil || [repositorio.repositAutorNome isEqual:[NSNull null]]) {
                repositorio.repositAutorNome = @"";
            }
            cell.nomeUsuarioRepositorio.text = repositorio.repositAutorNome;
            cell.nomeSobrenomeRepositorio.text = repositorio.repositAutorNome;
            cell.forkRepositorio.text = [[NSString alloc] initWithFormat:@"%d", repositorio.repositForks ];
            cell.starsRepositorio.text = [[NSString alloc] initWithFormat:@"%d", repositorio.repositStars ];
            if (repositorio.repositAutorFoto == nil || [repositorio.repositAutorNome isEqual:[NSNull null]]) {
            }else{
                [cell.imagemUsuarioRepositorio sd_setImageWithURL:[NSURL URLWithString:repositorio.repositAutorFoto] placeholderImage:[UIImage imageNamed:@"nuvemazul.png"]];
            }
        }
    } else {
            // motodo para fazer um scroll infinito ate que acabe a quantidade maxima de requisições
            if (self.carregandoDados.isHidden && ![Singleton sharedInstance].fimRequest) {
                [self getListaRepositorios];
        }
    }

    
    
    return cell;
}

//metodo que faz a requisição e uso da AFNetworking
-(void)getListaRepositorios{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    //incremento da page
    [Singleton sharedInstance].page += 1;
    
    NSString* urlPage = [[NSString alloc]initWithFormat:@"https://api.github.com/search/repositories?q=language:Java&sort=stars&page=%d", [Singleton sharedInstance].page];
    NSURL *URL = [NSURL URLWithString:urlPage];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error.localizedDescription);
            [self verifica: error.localizedDescription];
             self.carregandoDados.hidden = true;
            return;
        } else {
            
            self.carregandoDados.hidden = false;
        
            if (self.repositorios == nil) {
                self.repositorios =[[NSMutableArray alloc]init];
            }
            //Pega o os valores
            NSArray *resultArrayDictinary = [responseObject objectForKey:@"items"];
            
            //Verifica se acabaram os resultados
            if (resultArrayDictinary == nil) {
                [self verifica: @"vazio"];
            }else{
                [self initArray: resultArrayDictinary];
            }
        }
    }];
    [dataTask resume];
}
//passa o resuldato do JSon para um arry de objetos da classe Repositorio
-(void)initArray:(NSArray*) arrayResult{
    
    for (NSDictionary* atual in arrayResult) {
        Repositorio* repositorio = [[Repositorio alloc]init];
        if (repositorio != nil)
        {
            repositorio.repositNome = [atual valueForKey:@"name"];
            repositorio.repositDescricao = [atual valueForKey:@"description"];
            repositorio.repositAutorNome = [[atual valueForKey:@"owner"]  valueForKey:@"login"];
            repositorio.repositAutorFoto = [[atual valueForKey:@"owner"]  valueForKey:@"avatar_url"];
            repositorio.repositStars =  [[atual valueForKey:@"stargazers_count"] intValue];
            repositorio.repositForks = [[atual valueForKey:@"forks_count"] intValue];
            [self.repositorios addObject:repositorio];
        }
        
    }
    self.carregandoDados.hidden = true;
    [self.tableView reloadData];
}
//verifica a mensagem de erro e trata a mensagem para o usuario
-(void)verifica:(NSString*)string{
    if ([string  isEqual: @"vazio"] || [string isEqual:@"Request failed: client error (422)"]) {
        [self mensagemAlert:@"Oi, tudo bem? a API do GitHub só nos permite visualizar 1000 resuldados para saber mais acesse: https://developer.github.com/v3/search/" eTitulo:@"Ops!"];
        [Singleton sharedInstance].fimRequest = 1;
    }else{
        [self mensagemAlert:@"Oi, tudo bem? ocorreu um erro ao buscar mais informações." eTitulo:@"Ops!"];
    }

}

-(void)mensagemAlert: (NSString*) mensagem eTitulo: (NSString*) titulo{
    UIAlertController *alerta = [UIAlertController alertControllerWithTitle:titulo message:mensagem preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    
    [alerta addAction:ok];
    
    [self presentViewController: alerta animated: YES completion: nil];

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation

//passa dados selecionados
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"pepositSelecionado"]) {
        
        PullTableViewController *viewPull = [segue destinationViewController];
        Repositorio* repositorio =  [self.repositorios objectAtIndex: [self.tableView indexPathForSelectedRow].row];
        viewPull.repositSelecionado = [[NSString alloc] initWithFormat:@"%@/%@",repositorio.repositAutorNome,repositorio.repositNome];
        viewPull.titulo = repositorio.repositNome;
    }
}


@end
