//
//  Singleton.m
//  FernandaMelo-Teste
//
//  Created by Fernanda Melo on 24/07/16.
//  Copyright © 2016 Fernanda Melo. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton

+ (instancetype)sharedInstance{
    
    static dispatch_once_t pred = 0;
    static Singleton *_sharedInstance = nil;

    dispatch_once(&pred, ^{
        
        _sharedInstance = [[super alloc] init];
    });
    
    return _sharedInstance;
}


@end
